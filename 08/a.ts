import * as fs from "fs";

const input = fs.readFileSync('./08/input', 'utf8');
const screenWidth = 50;
const screenHeight = 6;

// const input = `
// rect 3x2
// rotate column x=1 by 1
// rotate row y=0 by 4
// rotate column x=1 by 1
// `;
// const screenWidth = 7;
// const screenHeight = 3;

let screen: string[][] = ('.'.repeat(screenWidth) + '\n').repeat(screenHeight).split('\n').map((line) => line.split('')).filter((line) => line.length);

input.trim().split('\n').forEach((line) => {
    let oldScreen = JSON.parse(JSON.stringify(screen));
    let words: string[] = line.split(' ');
    switch(words[0]) {
        case 'rect':
            let x: number = parseInt(words[1].split('x')[0]);
            let y: number = parseInt(words[1].split('x')[1]);
            for (let i = 0; i < y; i++) {
                for (let j = 0; j < x; j++) {
                    screen[i][j] = '#';
                }
            }
            break;
        case 'rotate':
            let i: number = parseInt(words[2].split('=')[1]);; 
            let offset: number = parseInt(words[4]);
            switch(words[1]) {
                case 'row':
                    for (let j = 0; j < screen[i].length; j++) { 
                        screen[i][(j + offset) % screen[i].length] = oldScreen[i][j];
                    }
                    break;
                case 'column':
                    for (let j = 0; j < screen.length; j++) { 
                        screen[(j + offset) % screen.length][i] = oldScreen[j][i];
                    }
                    break;
            }
            break;
    }
    console.log(screen.map((row) => row.join('')).join('\n'));
    console.log('\n');
});

console.log(
    screen.reduce(
        (accumulator, line) => accumulator + line.filter((c) => c === '#').length,
    0)
);