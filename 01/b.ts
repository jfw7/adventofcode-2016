const input = `R3, L5, R2, L1, L2, R5, L2, R2, L2, L2, L1, R2, L2, R4, R4, R1, L2, L3, R3, L1, R2, L2, L4, R4, R5, L3, R3, L3, L3, R4, R5, L3, R3, L5, L1, L2, R2, L1, R3, R1, L1, R187, L1, R2, R47, L5, L1, L2, R4, R3, L3, R3, R4, R1, R3, L1, L4, L1, R2, L1, R4, R5, L1, R77, L5, L4, R3, L2, R4, R5, R5, L2, L2, R2, R5, L2, R194, R5, L2, R4, L5, L4, L2, R5, L3, L2, L5, R5, R2, L3, R3, R1, L4, R2, L1, R5, L1, R5, L1, L1, R3, L1, R5, R2, R5, R5, L4, L5, L5, L5, R3, L2, L5, L4, R3, R1, R1, R4, L2, L4, R5, R5, R4, L2, L2, R5, R5, L5, L2, R4, R4, L4, R1, L3, R1, L1, L1, L1, L4, R5, R4, L4, L4, R5, R3, L2, L2, R3, R1, R4, L3, R1, L4, R3, L3, L2, R2, R2, R2, L1, L4, R3, R2, R2, L3, R2, L3, L2, R4, L2, R3, L4, R5, R4, R1, R5, R3`;

let direction: number = 0;
let x: number = 0;
let y: number = 0;
let visited: Array<string> = [];

input.trim().split(', ').forEach((item) => {
    let turn: string = item.substr(0, 1);
    let distance: number = parseInt(item.substr(1));
    direction = (direction + (item[0] === 'R' ? 1 : -1)) & 3;
    
    switch (direction) {
        case 0:
            for(let i: number = 1; i <= distance; i++) {
                x += 1;
                let location: string = `${x},${y}`;
                if (visited.indexOf(location) !== -1) {
                    console.log(Math.abs(x) + Math.abs(y));
                } else {
                    visited.push(location);
                }
            }
            break;
        case 1:
            for(let i: number = 1; i <= distance; i++) {
                y += 1;
                let location: string = `${x},${y}`;
                if (visited.indexOf(location) !== -1) {
                    console.log(Math.abs(x) + Math.abs(y));
                } else {
                    visited.push(location);
                }
            }
            break;
        case 2:
            for(let i: number = 1; i <= distance; i++) {
                x -= 1;
                let location: string = `${x},${y}`;
                if (visited.indexOf(location) !== -1) {
                    console.log(Math.abs(x) + Math.abs(y));
                } else {
                    visited.push(location);
                }
            }
            break;
        case 3:
            for(let i: number = 1; i <= distance; i++) {
                y -= 1;
                let location: string = `${x},${y}`;
                if (visited.indexOf(location) !== -1) {
                    console.log(Math.abs(x) + Math.abs(y));
                } else {
                    visited.push(location);
                }
            }
            break;
    }
});

