const input = 3001330;

let elves = [...Array(input + 1).keys()];
elves.shift();

let index = 0;

while (elves.length > 1) {
    let victimIndex = ((index + Math.floor(elves.length / 2)) % elves.length);
    index = (index + 1) % elves.length;
    for(let i = victimIndex; i < elves.length - 1; i++) {
        elves[i] = elves[i + 1];
    }
    elves.pop();
}

console.log(elves[0]);
