for(let input = 1; input < 16; input++) {
    let elves = [...Array(input + 1).keys()];
    elves.shift();

    let index = 0;


    while (elves.length > 1) {
        let nextIndex = ((index + 1) % elves.length);
        elves = elves.filter((e, i) => i !== nextIndex);
        index = nextIndex;
    }

    console.log(elves[0]);
}

// 1,1,3,1,3,5,7,1,3...
// http://oeis.org/A006257
// used binary rotate method to solve for input = 3001330