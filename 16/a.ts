const input = `10011111011011001`;
const diskLength = 272;

let data = input;

while (data.length < diskLength) {
    data = (data + '0' + data.split('').reverse().join('').replace(/[01]/g, (m) => ({'0': '1', '1': '0'}[m]))).substr(0, diskLength);
}

let checksum = data.split('');

do {
    checksum = checksum.map((c, i) => i % 2 ? null : (checksum[i] === checksum[i + 1] ? '1' : '0')).filter((e) => e);
} while (checksum.length % 2 === 0);

console.log(checksum.join(''));