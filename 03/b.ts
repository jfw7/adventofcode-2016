import * as fs from "fs";

const input = fs.readFileSync('./03/input', 'utf8');

let lines = input.split('\n').map((line) => line.trim().split(/\s+/).map((e) => parseInt(e)));
let cols = [];
for(let i = 0; i < lines.length; i += 3) {
    for (let j = 0; j < 3; j++) {
        cols.push([lines[i][j], lines[i+1][j], lines[i+2][j]]);
    }
}

console.log(
    cols.filter((sides) => {
        return (sides[0] + sides[1] > sides[2] && sides[0] + sides[2] > sides[1] && sides[1] + sides[2] > sides[0]);
    }).length
);
