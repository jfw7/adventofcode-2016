import * as fs from "fs";

const input = fs.readFileSync('./03/input', 'utf8');

console.log(
    input.split('\n').filter((line) => {
        let sides = line.trim().split(/\s+/).map((e) => parseInt(e));
        return (sides[0] + sides[1] > sides[2] && sides[0] + sides[2] > sides[1] && sides[1] + sides[2] > sides[0]);
    }).length
);
