import * as fs from "fs";

const input = fs.readFileSync('./07/input', 'utf8');

const abbaMatches: RegExp = new RegExp(/(\w)(\w)\2\1/g);

console.log(
    input.trim().split('\n').filter((ip) => {
        let hypernets = ip.match(/\[.+?]/g);
        if ((hypernets || []).filter((hypernet) => (hypernet.match(abbaMatches) || []).filter((abba) => { return abba[0] !== abba[1]}).length).length) {
            return false;
        }
        let abbas = ip.match(abbaMatches);
        if ((abbas || []).filter((abba) => {return abba[0] !== abba[1]}).length) {
            return true;
        }
        return false;
    }).length
);