import * as fs from "fs";

const input = fs.readFileSync('./07/input', 'utf8');

const abaMatches: RegExp = new RegExp(/(\w)\w\1/g);

console.log(
    input.trim().split('\n').map(ip => ip.split(/\[|]/)).filter((ipSegments) => {
        let supernets = ipSegments.filter((e, i) => i % 2 === 0).join();
        let hypernets = ipSegments.filter((e, i) => i % 2 === 1).join();

        let abas: string[] = [];
        let aba;
        while (aba = abaMatches.exec(supernets)) {
            if (aba[0] !== aba[1]) {
                abas.push(aba[0]);
                abaMatches.lastIndex = aba.index + 1;
            }
        }
        for (let i = 0; i < abas.length; i++) {
            let aba = abas[i];
            let babMatch = new RegExp(`${aba[1]}${aba[0]}${aba[1]}`);
            if (hypernets.match(babMatch)) {
                return true;
            }
        }
        return false;
    }).length
);