import * as fs from "fs";

const input = fs.readFileSync('./06/input', 'utf8');

console.log(
    input.trim().split('\n')
        .reduce(
            (accumulator, current) => accumulator.map((item, index) => item.concat([current[index]])),
            Array(8).fill([])
        ).map(
            (position: Array<string>) => position.sort((a, b) => position.filter(v => v === a).length - position.filter(v => v === b).length).pop()
        ).join('')
);