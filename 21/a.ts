import * as fs from "fs";

const input = fs.readFileSync('./21/input', 'utf8');

let password = 'abcdefgh'.split('')

input.trim().split('\n').forEach(line => {
    const words = line.split(' ');
    switch (words[0]) {
        case 'swap':
            switch (words[1]) {
                case 'position':
                    let swapPosX: number = parseInt(words[2]);
                    let swapPosY: number = parseInt(words[5]);
                    [password[swapPosX], password[swapPosY]] = [password[swapPosY], password[swapPosX]];
                    break;
                case 'letter':
                    let swapLetterX: number = password.indexOf(words[2]);
                    let swapLetterY: number = password.indexOf(words[5]);
                    [password[swapLetterX], password[swapLetterY]] = [password[swapLetterY], password[swapLetterX]];
                    break;
            }
            break;
        case 'rotate':
            let rotateCount: number;
            let rotateDirection: number = -1;
            switch (words[1]) {
                case 'based':
                    let rotateBasedX: number = password.indexOf(words[6]);
                    rotateCount = rotateBasedX + (rotateBasedX >= 4 ? 2 : 1);
                    break;
                case 'left':
                    rotateDirection *= -1;
                    rotateCount = parseInt(words[2]);
                    break;
                case 'right':
                    rotateCount = parseInt(words[2]);
                    break;
            }
            password = password.map((e, i) => password[(password.length * 2 + i + (rotateCount * rotateDirection)) % password.length])
            break;
        case 'reverse':
            let reverseX: number = parseInt(words[2]);
            let reverseY: number = parseInt(words[4]);
            password = password.slice(0, reverseX).concat(password.slice(reverseX, reverseY + 1).reverse(), password.slice(reverseY + 1));
            break;
        case 'move':
            let moveX: number = parseInt(words[2]);
            let moveY: number = parseInt(words[5]);
            let removed = password.splice(moveX, 1)[0];
            password.splice(moveY, 0, removed);
            break;
    }
})

console.log(password.join(''));