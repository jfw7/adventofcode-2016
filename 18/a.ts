const input = `.^^^^^.^^^..^^^^^...^.^..^^^.^^....^.^...^^^...^^^^..^...^...^^.^.^.......^..^^...^.^.^^..^^^^^...^.`;
const numRows = 40;

// const input =  `.^^.^.^^^^`;
// const numRows = 10;

let room = [input];

for (let i = 0; i < numRows - 1; i++) {
    let prevRow = '.' + room[i] + '.';
    let newRow = '';
    for(let j = 1; j <= input.length; j++) {
        switch (prevRow.substr(j - 1, 3)) {
            case '^..':
            case '^^.':
            case '..^':
            case '.^^':
                newRow += '^';
                break;
            default:
                newRow += '.';
                break;
        }
    }
    room.push(newRow);
}

console.log(room.reduce(((accumulator, current) => accumulator + current.split('').filter(c => c === '.').length), 0));