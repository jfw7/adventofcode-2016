var md5 = require('md5');

const input = 'cxdnnyjw';

let password: string = '';
for(let i: number = 0; password.length < 8; i++) {
    let hash: string = md5(input + i);
    if (hash.substr(0, 5) === '00000') {
        password += hash[5];
    }
}

console.log(password);