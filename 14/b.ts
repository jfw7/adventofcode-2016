var md5 = require('md5');

const input = 'ihaygndm';

let keys: Array<number> = [];

function md5_2016(input: string): string {
    let output: string = input;
    for (let i: number = 0; i < 2017; i++) {
        output = md5(output);
    }
    return output;
}

let hashes: Array<string> = Array.from(Array(100000));

for (let i: number = 0; keys.length < 64; i++) {
    if (! hashes[i]) {
        hashes[i] = md5_2016(input + i);
    }
    let matches = hashes[i].match(/(\w)\1\1/);
    if (matches) {
        let match = matches[0];
        for (let j: number = i + 1; j <= i + 1000; j++) {
            if (! hashes[j]) {
                hashes[j] = md5_2016(input + j);
            }
            if (hashes[j].match(match + match[0] + match[0])) {
                keys.push(i);
                break;
            }
        }
    }
}

console.log(keys);