import * as fs from "fs";

const input = fs.readFileSync('./20/input', 'utf8');

let ranges = input.trim().split('\n')
    .map(range => range.split('-').map(e => parseInt(e)))
    .sort((a, b) => a[0] - b[0]);

let count = 0;
for(let i = 0; i <= 4294967295; i++) {
    while (ranges[0][1] < i) {
        ranges.shift();
    }
    if (i < ranges[0][0] || i > ranges[0][1]) {
        count++;
    }
}

console.log(count);