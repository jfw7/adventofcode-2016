import * as fs from "fs";

const input = fs.readFileSync('./20/input', 'utf8');

console.log(
    input.trim().split('\n')
        .map(range => range.split('-').map(e => parseInt(e)))
        .sort((a, b) => a[0] - b[0])
);

// visual inspection found 14975795 quickly