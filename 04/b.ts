import * as fs from "fs";

const input = fs.readFileSync('./04/input', 'utf8');

input.split('\n').reduce((accumulator, item) => {
    let sections = item.split('-');
    let [id, checksum] = sections.pop().slice(0, -1).split('[');
    let name = sections.join('');
    let charCount = {};
    name.split('').forEach((c) => charCount[c] = charCount[c] + 1 || 1);
    let calculatedChecksum = Object.keys(charCount).sort((a, b) => {
        if (charCount[a] === charCount[b]) {
            return a.charCodeAt(0) - b.charCodeAt(0);
        } else {
            return charCount[b] - charCount[a];
        }
    }).join('').substr(0, 5);
    if (checksum === calculatedChecksum) {
        console.log(
            name.split('').map((c) => String.fromCharCode(((c.charCodeAt(0) - 97 + parseInt(id)) % 26) + 97)).join(''), id
        );
        return accumulator + parseInt(id);
    } else {
        return accumulator;
    }
}, 0)
